/**
 * App: Kancional
 * Author: Ondra Gersl, ondra.gersl@gmail.com
 */

/* Windows Phone viewport width fixup */
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
	var msViewportStyle = document.createElement('style')
	msViewportStyle.appendChild(
		document.createTextNode(
			'@-ms-viewport{width:auto!important}'
		)
	)
	document.querySelector('head').appendChild(msViewportStyle)
}

/* SVG fallback */
if (!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1")) {
	var imgs = document.getElementsByClassName('song');
	var endsWithDotSvg = /.*\.svg$/
	var i = 0;
	var l = imgs.length;
	for(; i != l; ++i) {
		if(imgs[i].src.match(endsWithDotSvg)) {
			imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
		}
	}
}
